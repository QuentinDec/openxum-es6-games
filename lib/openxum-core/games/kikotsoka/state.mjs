"use strict";

const State = {
  VACANT: 0,
  BLACK: 1,
  WHITE: 2,
  BLACK_BLOCKED: 3,
  WHITE_BLOCKED: 4,
  BLACK_SHIDO: 5,
  WHITE_SHIDO: 6,
  BLACK_SHIDO_BLOCKED: 7,
  WHITE_SHIDO_BLOCKED: 8,
  BLOCKED_IN_BLACK: 9,
  BLOCKED_IN_WHITE: 10,
  to_string(s) {
    if (s === State.VACANT) { return '[ ]'; }
    if (s === State.BLACK) { return '[B]'; }
    if (s === State.WHITE) { return '[W]'; }
    if (s === State.BLACK_BLOCKED) { return ' b '; }
    if (s === State.WHITE_BLOCKED) { return ' w '; }
    if (s === State.BLACK_SHIDO) { return '*b*'; }
    if (s === State.WHITE_SHIDO) { return '*w*'; }
    if (s === State.BLACK_SHIDO_BLOCKED) { return '+b+'; }
    if (s === State.WHITE_SHIDO_BLOCKED) { return '+w+'; }
    if (s === State.BLOCKED_IN_BLACK || s === State.BLOCKED_IN_WHITE) { return '   '; }
  }
};

export default State;