"use strict";

import Color from './color.mjs';
import Coordinates from './coordinates.mjs';
import GameType from './game_type.mjs';
import Intersection from './intersection.mjs';
import MarbleColor from './marble_color.mjs';
import Move from './move.mjs';
import MoveType from './move_type.mjs';
import OpenXum from '../../openxum/index.mjs';
import Phase from './phase.mjs';
import State from './state.mjs';

// grid constants definition
const begin_number = [1, 1, 1, 1, 2, 3, 4];
const end_number = [4, 5, 6, 7, 7, 7, 7];
const letter_matrix = [
  'X', 'X', 'Z', 'X', 'D', 'X', 'X', 'X',
  'X', 'Z', 'X', 'C', 'X', 'E', 'X', 'X',
  'Z', 'X', 'B', 'X', 'D', 'X', 'F', 'X',
  'X', 'A', 'X', 'C', 'X', 'E', 'X', 'G',
  'Z', 'X', 'B', 'X', 'D', 'X', 'F', 'X',
  'X', 'A', 'X', 'C', 'X', 'E', 'X', 'G',
  'Z', 'X', 'B', 'X', 'D', 'X', 'F', 'X',
  'X', 'A', 'X', 'C', 'X', 'E', 'X', 'G',
  'Z', 'X', 'B', 'X', 'D', 'X', 'F', 'X',
  'X', 'A', 'X', 'C', 'X', 'E', 'X', 'G',
  'X', 'X', 'B', 'X', 'D', 'X', 'F', 'X',
  'X', 'X', 'X', 'C', 'X', 'E', 'X', 'X',
  'X', 'X', 'X', 'X', 'D', 'X', 'X', 'X'
];
const number_matrix = [
  0, 0, 0, 0, 7, 0, 0, 0,
  0, 0, 0, 6, 0, 7, 0, 0,
  0, 0, 5, 0, 6, 0, 7, 0,
  0, 4, 0, 5, 0, 6, 0, 7,
  0, 0, 4, 0, 5, 0, 6, 0,
  0, 3, 0, 4, 0, 5, 0, 6,
  0, 0, 3, 0, 4, 0, 5, 0,
  0, 2, 0, 3, 0, 4, 0, 5,
  0, 0, 2, 0, 3, 0, 4, 0,
  0, 1, 0, 2, 0, 3, 0, 4,
  0, 0, 1, 0, 2, 0, 3, 0,
  0, 0, 0, 1, 0, 2, 0, 0,
  0, 0, 0, 0, 1, 0, 0, 0
];

// enums definition
const letters = ['A', 'B', 'C', 'D', 'E', 'F', 'G'];

class Engine extends OpenXum.Engine {
  constructor(t, c) {
    super();
    this._type = t;
    this._current_color = c;
    this._phase = Phase.SELECT_MARBLE_IN_POOL;
    this._blackMarbleNumber = 10;
    this._greyMarbleNumber = 8;
    this._whiteMarbleNumber = 6;
    this._capturedBlackMarbleNumber = [0, 0];
    this._capturedGreyMarbleNumber = [0, 0];
    this._capturedWhiteMarbleNumber = [0, 0];
    this._intersections = {};
    for (let i = 0; i < letters.length; ++i) {
      const l = letters[i];

      for (let n = begin_number[l.charCodeAt(0) - 'A'.charCodeAt(0)];
           n <= end_number[l.charCodeAt(0) - 'A'.charCodeAt(0)]; ++n) {
        const coordinates = new Coordinates(l, n);

        this._intersections[coordinates.hash()] = new Intersection(coordinates);
      }
    }
  }

// public methods
  build_move() {
    return new Move();
  }

  clone() {
    let o = new Engine(this._type, this._current_color);

    o.set(this._phase, this._intersections, this._blackMarbleNumber, this._greyMarbleNumber,
      this._whiteMarbleNumber, this._capturedBlackMarbleNumber, this._capturedGreyMarbleNumber,
      this._capturedWhiteMarbleNumber);
    return o;
  }

  current_color() {
    return this._current_color;
  }

  exist_intersection(letter, number) {
    const coordinates = new Coordinates(letter, number);

    if (coordinates.is_valid()) {
      return this._intersections[coordinates.hash()] !== null;
    } else {
      return false;
    }
  }

  get_name() {
    return 'Zertz';
  }

  get_possible_capturing_marbles(coordinates) {
    let list = [];
    const intersection = this._intersections[coordinates.hash()];

    if (intersection.marble_is_present()) {
      const letter = intersection.letter();
      const number = intersection.number();

      // letter + number increase
      if (this._get_intersection(letter, number + 1) && this._get_intersection(letter, number + 2) && this._get_intersection(letter, number + 1).marble_is_present() && this._get_intersection(letter, number + 2).state() === State.VACANT) {
        list.push(new Coordinates(letter, number + 1));
      }

      // letter + number decrease
      if (this._get_intersection(letter, number - 1) && this._get_intersection(letter, number - 2) && this._get_intersection(letter, number - 1).marble_is_present() && this._get_intersection(letter, number - 2).state() === State.VACANT) {
        list.push(new Coordinates(letter, number - 1));
      }

      // letter increase + number
      if (this._get_intersection(String.fromCharCode(letter.charCodeAt(0) + 1), number) && this._get_intersection(String.fromCharCode(letter.charCodeAt(0) + 2), number) && this._get_intersection(String.fromCharCode(letter.charCodeAt(0) + 1), number).marble_is_present() && this._get_intersection(String.fromCharCode(letter.charCodeAt(0) + 2), number).state() === State.VACANT) {
        list.push(new Coordinates(String.fromCharCode(letter.charCodeAt(0) + 1), number));
      }

      // letter decrease + number
      if (this._get_intersection(String.fromCharCode(letter.charCodeAt(0) - 1), number) && this._get_intersection(String.fromCharCode(letter.charCodeAt(0) - 2), number) && this._get_intersection(String.fromCharCode(letter.charCodeAt(0) - 1), number).marble_is_present() && this._get_intersection(String.fromCharCode(letter.charCodeAt(0) - 2), number).state() === State.VACANT) {
        list.push(new Coordinates(String.fromCharCode(letter.charCodeAt(0) - 1), number));
      }

      // letter increase + number increase
      if (this._get_intersection(String.fromCharCode(letter.charCodeAt(0) + 1), number + 1) && this._get_intersection(String.fromCharCode(letter.charCodeAt(0) + 2), number + 2) && this._get_intersection(String.fromCharCode(letter.charCodeAt(0) + 1), number + 1).marble_is_present() && this._get_intersection(String.fromCharCode(letter.charCodeAt(0) + 2), number + 2).state() === State.VACANT) {
        list.push(new Coordinates(String.fromCharCode(letter.charCodeAt(0) + 1), number + 1));
      }

      // letter decrease + number decrease
      if (this._get_intersection(String.fromCharCode(letter.charCodeAt(0) - 1), number - 1) && this._get_intersection(String.fromCharCode(letter.charCodeAt(0) - 2), number - 2) && this._get_intersection(String.fromCharCode(letter.charCodeAt(0) - 1), number - 1).marble_is_present() && this._get_intersection(String.fromCharCode(letter.charCodeAt(0) - 2), number - 2).state() === State.VACANT) {
        list.push(new Coordinates(String.fromCharCode(letter.charCodeAt(0) - 1), number - 1));
      }
    }
    return list;
  }

  get_possible_move_list() {
    let list = [];

    if (this._phase === Phase.REMOVE_RING) {
      const coordinates = this.get_possible_removing_rings();

      coordinates.forEach((to) => {
        list.push(new Move(MoveType.REMOVE_RING, this._current_color, to));
      });
    } else if (this._phase === Phase.CAPTURE) {
      Object.keys(this._intersections).map((key, index) => {
        const from = this._intersections[key];

        if (from.marble_is_present()) {
          const coordinates = this.get_possible_capturing_marbles(from.coordinates());

          coordinates.forEach((to) => {
            list.push(new Move(MoveType.CAPTURE, this._current_color, to, from.color(), from.coordinates()));
          });
        }
      });
    } else if (this._phase === Phase.SELECT_MARBLE_IN_POOL) {
      const coordinates = this._get_free_rings();

      coordinates.forEach((to) => {
        for (let i = 0; i < this._blackMarbleNumber; ++i) {
          list.push(new Move(MoveType.PUT_MARBLE, this._current_color, to, MarbleColor.BLACK));
        }
        for (let i = 0; i < this._greyMarbleNumber; ++i) {
          list.push(new Move(MoveType.PUT_MARBLE, this._current_color, to, MarbleColor.GREY));
        }
        for (let i = 0; i < this._whiteMarbleNumber; ++i) {
          list.push(new Move(MoveType.PUT_MARBLE, this._current_color, to, MarbleColor.WHITE));
        }
      });
    }

    if (list.length === 0) {
      console.log("ERROR", this._phase);
      console.log(this.to_string());
    }

    return list;
  }

  get_possible_removing_rings() {
    let list = [];

    Object.keys(this._intersections).forEach((index) => {
      const intersection = this._intersections[index];
      let neighbour = null;
      let next_neighbour;

      if (intersection.state() === State.VACANT) {
        const letter = intersection.letter();
        const number = intersection.number();

        // letter + number increase
        neighbour = this._get_intersection(letter, number + 1);
        if (neighbour === null || (neighbour && neighbour.state() === State.EMPTY)) {
          next_neighbour = this._get_intersection(String.fromCharCode(letter.charCodeAt(0) + 1), number + 1);
          if (next_neighbour === null || (next_neighbour && next_neighbour.state() === State.EMPTY)) {
            list.push(intersection.coordinates());
          }
        }

        // letter + number decrease
        neighbour = this._get_intersection(letter, number - 1);
        if (neighbour === null || (neighbour && neighbour.state() === State.EMPTY)) {
          next_neighbour = this._get_intersection(String.fromCharCode(letter.charCodeAt(0) - 1), number - 1);
          if (next_neighbour === null || (next_neighbour && next_neighbour.state() === State.EMPTY)) {
            list.push(intersection.coordinates());
          }
        }

        // letter increase + number
        neighbour = this._get_intersection(String.fromCharCode(letter.charCodeAt(0) + 1), number);
        if (neighbour === null || (neighbour && neighbour.state() === State.EMPTY)) {
          next_neighbour = this._get_intersection(letter, number - 1);
          if (next_neighbour === null || (next_neighbour && next_neighbour.state() === State.EMPTY)) {
            list.push(intersection.coordinates());
          }
        }

        // letter decrease + number
        neighbour = this._get_intersection(String.fromCharCode(letter.charCodeAt(0) - 1), number);
        if (neighbour === null || (neighbour && neighbour.state() === State.EMPTY)) {
          next_neighbour = this._get_intersection(letter, number + 1);
          if (next_neighbour === null || (next_neighbour && next_neighbour.state() === State.EMPTY)) {
            list.push(intersection.coordinates());
          }
        }

        // letter increase + number increase
        neighbour = this._get_intersection(String.fromCharCode(letter.charCodeAt(0) + 1), number + 1);
        if (neighbour === null || (neighbour && neighbour.state() === State.EMPTY)) {
          next_neighbour = this._get_intersection(String.fromCharCode(letter.charCodeAt(0) + 1), number);
          if (next_neighbour === null || (next_neighbour && next_neighbour.state() === State.EMPTY)) {
            list.push(intersection.coordinates());
          }
        }

        // letter decrease + number decrease
        neighbour = this._get_intersection(String.fromCharCode(letter.charCodeAt(0) - 1), number - 1);
        if (neighbour === null || (neighbour && neighbour.state() === State.EMPTY)) {
          next_neighbour = this._get_intersection(String.fromCharCode(letter.charCodeAt(0) - 1), number);
          if (next_neighbour === null || (next_neighbour && next_neighbour.state() === State.EMPTY)) {
            list.push(intersection.coordinates());
          }
        }
      }
    });
    return list;
  }

  get_type() {
    return this._type;
  }

  is_finished() {
    return (this._blackMarbleNumber === 0 && this._greyMarbleNumber === 0 && this._whiteMarbleNumber === 0) ||
      this._is_possible_to_remove_ring() ||
      this._is_finished(Color.ONE) || this._is_finished(Color.TWO);
  }

  is_possible_to_capture_with(intersection) {
    if (intersection.marble_is_present()) {
      const letter = intersection.letter();
      const number = intersection.number();

      // letter + number increase
      if (this._get_intersection(letter, number + 1) && this._get_intersection(letter, number + 2) && this._get_intersection(letter, number + 1).marble_is_present() && this._get_intersection(letter, number + 2).state() === State.VACANT) {
        return true;
      }

      // letter + number decrease
      if (this._get_intersection(letter, number - 1) && this._get_intersection(letter, number - 2) && this._get_intersection(letter, number - 1).marble_is_present() && this._get_intersection(letter, number - 2).state() === State.VACANT) {
        return true;
      }

      // letter increase + number
      if (this._get_intersection(String.fromCharCode(letter.charCodeAt(0) + 1), number) && this._get_intersection(String.fromCharCode(letter.charCodeAt(0) + 2), number) && this._get_intersection(String.fromCharCode(letter.charCodeAt(0) + 1), number).marble_is_present() && this._get_intersection(String.fromCharCode(letter.charCodeAt(0) + 2), number).state() === State.VACANT) {
        return true;
      }

      // letter decrease + number
      if (this._get_intersection(String.fromCharCode(letter.charCodeAt(0) - 1), number) && this._get_intersection(String.fromCharCode(letter.charCodeAt(0) - 2), number) && this._get_intersection(String.fromCharCode(letter.charCodeAt(0) - 1), number).marble_is_present() && this._get_intersection(String.fromCharCode(letter.charCodeAt(0) - 2), number).state() === State.VACANT) {
        return true;
      }

      // letter increase + number increase
      if (this._get_intersection(String.fromCharCode(letter.charCodeAt(0) + 1), number + 1) && this._get_intersection(String.fromCharCode(letter.charCodeAt(0) + 2), number + 2) && this._get_intersection(String.fromCharCode(letter.charCodeAt(0) + 1), number + 1).marble_is_present() && this._get_intersection(String.fromCharCode(letter.charCodeAt(0) + 2), number + 2).state() === State.VACANT) {
        return true;
      }

      // letter decrease + number decrease
      if (this._get_intersection(String.fromCharCode(letter.charCodeAt(0) - 1), number - 1) && this._get_intersection(String.fromCharCode(letter.charCodeAt(0) - 2), number - 2) && this._get_intersection(String.fromCharCode(letter.charCodeAt(0) - 1), number - 1).marble_is_present() && this._get_intersection(String.fromCharCode(letter.charCodeAt(0) - 2), number - 2).state() === State.VACANT) {
        return true;
      }
    }
  }

  is_possible_capturing_marble_with(origin, capturing) {
    return this._belong_to2(capturing, this.get_possible_capturing_marbles(origin));
  }

  is_possible_capturing_marbles(coordinates) {
    return this.is_possible_to_capture_with(this._intersections[coordinates.hash()]);
  }

  move(move) {
    if (move.type() === MoveType.PUT_MARBLE) {
      this._put_marble(move.to(), move.marble_color(), move.color());
    } else if (move.type() === MoveType.REMOVE_RING) {
      this._remove_ring(move.to(), move.color());
    } else if (move.type() === MoveType.CAPTURE) {
      this._capture(move.from(), move.to(), move.color());
    }
  }

  parse(str) {
    // TODO
  }

  phase() {
    return this._phase;
  }

  to_string() {
    let str = "                   7\n";
    let k = 6;

    for (let j = 0; j < 13; ++j) {
      for (let i = 0; i < 8; ++i) {
        const l = letter_matrix[i + 8 * j];
        const n = number_matrix[i + 8 * j];

        if (l !== 'X' && l !== 'Z') {
          const coordinates = new Coordinates(l, n);
          const intersection = this._intersections[coordinates.hash()];

          str += intersection.to_string();
        } else if (l === 'Z') {
          let s = "    ";

          s += k;
          str += s;
          --k;
        } else {
          str += "     ";
        }
      }
      str += "\n";
    }
    str += "      A    B    C    D    E    F    G\n";
    str += " One : ";
    str += this._capturedBlackMarbleNumber[0];
    str += " ";
    str += this._capturedGreyMarbleNumber[0];
    str += " ";
    str += this._capturedWhiteMarbleNumber[0];
    str += "\n";
    str += " Two : ";
    str += this._capturedBlackMarbleNumber[1];
    str += " ";
    str += this._capturedGreyMarbleNumber[1];
    str += " ";
    str += this._capturedWhiteMarbleNumber[1];
    str += "\n";
    return str;
  }

  verify_remove_ring(coordinates) {
    var list = this.get_possible_removing_rings();

    for (let index = 0; index < list.length; ++index) {
      if (list[index].hash() === coordinates.hash()) {
        return true;
      }
    }
    return false;
  }

  winner_is() {
    if (this._is_possible_to_remove_ring() || (this._blackMarbleNumber === 0 && this._greyMarbleNumber === 0 && this._whiteMarbleNumber === 0)) {
      return Color.NONE;
    } else {
      if (this._type === GameType.BLITZ) {
        if ((this._capturedBlackMarbleNumber[0] === 2 &&
          this._capturedGreyMarbleNumber[0] === 2 &&
          this._capturedWhiteMarbleNumber[0] === 2) ||
          (this._capturedBlackMarbleNumber[0] === 5 ||
            this._capturedGreyMarbleNumber[0] === 4 ||
            this._capturedWhiteMarbleNumber[0] === 3)) {
          return Color.ONE;
        } else {
          return Color.TWO;
        }
      } else { // type = GameType.REGULAR
        if ((this._capturedBlackMarbleNumber[0] === 3 &&
          this._capturedGreyMarbleNumber[0] === 3 &&
          this._capturedWhiteMarbleNumber[0] === 3) ||
          (this._capturedBlackMarbleNumber[0] === 6 ||
            this._capturedGreyMarbleNumber[0] === 5 ||
            this._capturedWhiteMarbleNumber[0] === 4)) {
          return Color.ONE;
        } else {
          return Color.TWO;
        }
      }
    }
  }

// private methods
  _belong_to(element, list) {
    for (let index = 0; index < list.length; ++index) {
      if (list[index].coordinates().hash() === element.coordinates().hash()) {
        return true;
      }
    }
    return false;
  }

  _belong_to2(element, list) {
    for (let index = 0; index < list.length; ++index) {
      if (list[index].hash() === element.hash()) {
        return true;
      }
    }
    return false;
  }

  _can_capture() {
    return this._get_can_capture_marbles().length > 0;
  }

  _capture_marble(intersection, player) {
    if (intersection.state() === State.BLACK_MARBLE) {
      ++this._capturedBlackMarbleNumber[player];
    } else if (intersection.state() === State.GREY_MARBLE) {
      ++this._capturedGreyMarbleNumber[player];
    } else {
      ++this._capturedWhiteMarbleNumber[player];
    }
    intersection.remove_marble();
  }

  _capture(origin, captured, player) {
    const destination = this._get_destination(origin, captured);
    const ito = this._intersections[origin.hash()];
    const itd = this._intersections[destination.hash()];
    const color = ito.color();

    ito.remove_marble();
    this._capture_marble(this._intersections[captured.hash()], player);
    itd.put_marble(color);
    if (!this._is_possible_to_capture_with(itd)) {
      this._change_color();
      if (this._can_capture()) {
        this._phase = Phase.CAPTURE;
      } else {
        this._phase = Phase.SELECT_MARBLE_IN_POOL;
      }
    }
  }

  _capture_marble_and_ring(captured, player) {
    for (let index = 0; index < captured.length; ++index) {
      const intersection = this._intersections[captured[index].hash()];

      this._capture_marble(intersection, player);
      intersection.remove_ring();
    }
    this._change_color();
  }

  _change_color() {
    this._current_color = this._current_color === Color.ONE ? Color.TWO : Color.ONE;
  }

  _get_can_capture_marbles() {
    let list = [];

    Object.keys(this._intersections).forEach((index) => {
      const intersection = this._intersections[index];

      if (this._is_possible_to_capture_with(intersection)) {
        list.push(intersection.coordinates());
      }
    });
    return list;
  }

  _get_destination(origin, captured) {
    const delta_letter = captured.letter().charCodeAt(0) - origin.letter().charCodeAt(0);
    const delta_number = captured.number() - origin.number();

    return new Coordinates(String.fromCharCode(captured.letter().charCodeAt(0) + delta_letter), captured.number() + delta_number);
  }

  _get_free_rings() {
    let list = [];

    Object.keys(this._intersections).forEach((index) => {
      const intersection = this._intersections[index];

      if (intersection.state() === State.VACANT) {
        list.push(intersection.coordinates());
      }
    });
    return list;
  }

  _get_intersection(letter, number) {
    const coordinates = new Coordinates(letter, number);

    if (coordinates.is_valid()) {
      return this._intersections[coordinates.hash()];
    } else {
      return null;
    }
  }

  _get_isolated_marbles() {
    let list = [];

    Object.keys(this._intersections).forEach((index) => {
      const intersection = this._intersections[index];

      if (this._is_isolated_marble(intersection)) {
        list.push(intersection.coordinates());
      }
    });
    return list;
  }

  _is_finished(player) {
    if (this._type === GameType.BLITZ) {
      if (player === Color.ONE) {
        return (this._capturedBlackMarbleNumber[0] === 2 &&
          this._capturedGreyMarbleNumber[0] === 2 &&
          this._capturedWhiteMarbleNumber[0] === 2) ||
          (this._capturedBlackMarbleNumber[0] === 5 ||
            this._capturedGreyMarbleNumber[0] === 4 ||
            this._capturedWhiteMarbleNumber[0] === 3);
      } else {
        return (this._capturedBlackMarbleNumber[1] === 2 &&
          this._capturedGreyMarbleNumber[1] === 2 &&
          this._capturedWhiteMarbleNumber[1] === 2) ||
          (this._capturedBlackMarbleNumber[1] === 5 ||
            this._capturedGreyMarbleNumber[1] === 4 ||
            this._capturedWhiteMarbleNumber[1] === 3);
      }
    } else { // type = GameType.REGULAR
      if (player === Color.ONE) {
        return (this._capturedBlackMarbleNumber[0] === 3 &&
          this._capturedGreyMarbleNumber[0] === 3 &&
          this._capturedWhiteMarbleNumber[0] === 3) ||
          (this._capturedBlackMarbleNumber[0] === 6 ||
            this._capturedGreyMarbleNumber[0] === 5 ||
            this._capturedWhiteMarbleNumber[0] === 4);
      } else {
        return (this._capturedBlackMarbleNumber[1] === 3 &&
          this._capturedGreyMarbleNumber[1] === 3 &&
          this._capturedWhiteMarbleNumber[1] === 3) ||
          (this._capturedBlackMarbleNumber[1] === 6 ||
            this._capturedGreyMarbleNumber[1] === 5 ||
            this._capturedWhiteMarbleNumber[1] === 4);
      }
    }
  }

  _is_isolated_marble(intersection) {
    if (intersection.marble_is_present()) {
      let list = [];
      let visited = [];
      let stop = false;

      list.push(intersection);
      while (list.length > 0 && !stop) {
        const current = list.pop();
        const letter = current.letter();
        const number = current.number();
        const N = this._get_intersection(letter, number + 1);
        const NE = this._get_intersection(String.fromCharCode(letter.charCodeAt(0) + 1), number + 1);
        const SE = this._get_intersection(String.fromCharCode(letter.charCodeAt(0) + 1), number);
        const S = this._get_intersection(letter, number - 1);
        const SO = this._get_intersection(String.fromCharCode(letter.charCodeAt(0) - 1), number - 1);
        const NO = this._get_intersection(String.fromCharCode(letter.charCodeAt(0) - 1), number);

        visited.push(current);
        if (N !== null && N.state() === State.VACANT) {
          stop = true;
          break;
        } else if (N !== null && N.state() !== State.EMPTY) {
          if (!this._belong_to(N, list) && !this._belong_to(N, visited)) {
            list.push(N);
          }
        }
        if (NE !== null && NE.state() === State.VACANT) {
          stop = true;
          break;
        } else if (NE !== null && NE.state() !== State.EMPTY) {
          if (!this._belong_to(NE, list) && !this._belong_to(NE, visited)) {
            list.push(NE);
          }
        }
        if (SE !== null && SE.state() === State.VACANT) {
          stop = true;
          break;
        } else if (SE !== null && SE.state() !== State.EMPTY) {
          if (!this._belong_to(SE, list) && !this._belong_to(SE, visited)) {
            list.push(SE);
          }
        }
        if (S !== null && S.state() === State.VACANT) {
          stop = true;
          break;
        } else if (S !== null && S.state() !== State.EMPTY) {
          if (!this._belong_to(S, list) && !this._belong_to(S, visited)) {
            list.push(S);
          }
        }
        if (SO !== null && SO.state() === State.VACANT) {
          stop = true;
          break;
        } else if (SO !== null && SO.state() !== State.EMPTY) {
          if (!this._belong_to(SO, list) && !this._belong_to(SO, visited)) {
            list.push(SO);
          }
        }
        if (NO !== null && NO.state() === State.VACANT) {
          stop = true;
          break;
        } else if (NO !== null && NO.state() !== State.EMPTY) {
          if (!this._belong_to(NO, list) && !this._belong_to(NO, visited)) {
            list.push(NO);
          }
        }
      }
      return !stop;
    } else {
      return false;
    }
  }

  _is_possible_to_capture_with(intersection) {
    if (intersection.marble_is_present()) {
      const letter = intersection.letter();
      const number = intersection.number();

      // letter + number increase
      if (this._get_intersection(letter, number + 1) && this._get_intersection(letter, number + 2) && this._get_intersection(letter, number + 1).marble_is_present() && this._get_intersection(letter, number + 2).state() === State.VACANT) {
        return true;
      }

      // letter + number decrease
      if (this._get_intersection(letter, number - 1) && this._get_intersection(letter, number - 2) && this._get_intersection(letter, number - 1).marble_is_present() && this._get_intersection(letter, number - 2).state() === State.VACANT) {
        return true;
      }

      // letter increase + number
      if (this._get_intersection(String.fromCharCode(letter.charCodeAt(0) + 1), number) && this._get_intersection(String.fromCharCode(letter.charCodeAt(0) + 2), number) && this._get_intersection(String.fromCharCode(letter.charCodeAt(0) + 1), number).marble_is_present() && this._get_intersection(String.fromCharCode(letter.charCodeAt(0) + 2), number).state() === State.VACANT) {
        return true;
      }

      // letter decrease + number
      if (this._get_intersection(String.fromCharCode(letter.charCodeAt(0) - 1), number) && this._get_intersection(String.fromCharCode(letter.charCodeAt(0) - 2), number) && this._get_intersection(String.fromCharCode(letter.charCodeAt(0) - 1), number).marble_is_present() && this._get_intersection(String.fromCharCode(letter.charCodeAt(0) - 2), number).state() === State.VACANT) {
        return true;
      }

      // letter increase + number increase
      if (this._get_intersection(String.fromCharCode(letter.charCodeAt(0) + 1), number + 1) && this._get_intersection(String.fromCharCode(letter.charCodeAt(0) + 2), number + 2) && this._get_intersection(String.fromCharCode(letter.charCodeAt(0) + 1), number + 1).marble_is_present() && this._get_intersection(String.fromCharCode(letter.charCodeAt(0) + 2), number + 2).state() === State.VACANT) {
        return true;
      }

      // letter decrease + number decrease
      if (this._get_intersection(String.fromCharCode(letter.charCodeAt(0) - 1), number - 1) && this._get_intersection(String.fromCharCode(letter.charCodeAt(0) - 2), number - 2) && this._get_intersection(String.fromCharCode(letter.charCodeAt(0) - 1), number - 1).marble_is_present() && this._get_intersection(String.fromCharCode(letter.charCodeAt(0) - 2), number - 2).state() === State.VACANT) {
        return true;
      }
    }
  }

  _is_possible_to_remove_ring() {
    let ok = true;

    Object.keys(this._intersections).forEach((index) => {
      const intersection = this._intersections[index];

      if (intersection.state() !== State.EMPTY ||
        intersection.state() !== State.BLACK_MARBLE ||
        intersection.state() !== State.GREY_MARBLE ||
        intersection.state() !== State.WHITE_MARBLE) {
        ok = false;
        return;
      }
    });
    return ok;
  }

  _put_marble(coordinates, color) {
    const intersection = this._intersections[coordinates.hash()];

    intersection.put_marble(color);
    if (color === MarbleColor.BLACK) {
      --this._blackMarbleNumber;
    } else if (color === MarbleColor.GREY) {
      --this._greyMarbleNumber;
    } else {
      --this._whiteMarbleNumber;
    }
    if (this.get_possible_removing_rings().length > 0) {
      this._phase = Phase.REMOVE_RING;
    } else {
      this._change_color();
      if (this._can_capture()) {
        this._phase = Phase.CAPTURE;
      } else {
        this._phase = Phase.SELECT_MARBLE_IN_POOL;
      }
    }
  }

  _remove_ring(coordinates, player) {
    const intersection = this._intersections[coordinates.hash()];

    intersection.remove_ring();

    const list = this._get_isolated_marbles();

    if (list.length > 0) {
      this._capture_marble_and_ring(list, player);
    }
    this._change_color();
    if (this._can_capture()) {
      this._phase = Phase.CAPTURE;
    } else {
      this._phase = Phase.SELECT_MARBLE_IN_POOL;
    }
  }

  _set(phase, intersections, blackMarbleNumber, greyMarbleNumber, whiteMarbleNumber, capturedBlackMarbleNumber, capturedGreyMarbleNumber, capturedWhiteMarbleNumber) {
    Object.keys(intersections).map((key, index) => {
      this._intersections[key] = intersections[key].clone();
    });
    this._phase = phase;
    this._blackMarbleNumber = blackMarbleNumber;
    this._greyMarbleNumber = greyMarbleNumber;
    this._whiteMarbleNumber = whiteMarbleNumber;
    this._capturedBlackMarbleNumber = [];
    this._capturedBlackMarbleNumber[0] = capturedBlackMarbleNumber[0];
    this._capturedBlackMarbleNumber[1] = capturedBlackMarbleNumber[1];
    this._capturedGreyMarbleNumber = [];
    this._capturedGreyMarbleNumber[0] = capturedGreyMarbleNumber[0];
    this._capturedGreyMarbleNumber[1] = capturedGreyMarbleNumber[1];
    this._capturedWhiteMarbleNumber = [];
    this._capturedWhiteMarbleNumber[0] = capturedWhiteMarbleNumber[0];
    this._capturedWhiteMarbleNumber[1] = capturedWhiteMarbleNumber[1];
  }
}

export default Engine;