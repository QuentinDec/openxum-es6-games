"use strict";

import Kikotsoka from '../../../openxum-core/games/kikotsoka/index.mjs';
import AI from '../../../openxum-ai/index.mjs';
import Gui from './gui.mjs';
import Manager from './manager.mjs';

export default {
  Gui: Gui,
  Manager: Manager,
  Settings: {
    ai: {
      mcts: AI.Generic.RandomPlayer // AI.Specific.Kikotsoka.MCTSPlayer
    },
    colors: {
      first: Kikotsoka.Color.BLACK,
      init: Kikotsoka.Color.BLACK,
      list: [
        {key: Kikotsoka.Color.BLACK, value: 'black'},
        {key: Kikotsoka.Color.WHITE, value: 'white'}
      ]
    },
    modes: {
      init: Kikotsoka.GameType.SMALL,
      list: [
        {key: Kikotsoka.GameType.SMALL, value: 'small'},
        {key: Kikotsoka.GameType.MEDIUM, value: 'medium'},
        {key: Kikotsoka.GameType.LARGE, value: 'large'}
      ]
    },
    opponent_color(color) {
      return color === Kikotsoka.Color.BLACK ? Kikotsoka.Color.WHITE : Kikotsoka.Color.BLACK;
    },
    types: {
      init: 'ai',
      list: [
        {key: 'gui', value: 'GUI'},
        {key: 'ai', value: 'AI'},
        {key: 'online', value: 'Online'},
        {key: 'offline', value: 'Offline'}
      ]
    }
  }
};